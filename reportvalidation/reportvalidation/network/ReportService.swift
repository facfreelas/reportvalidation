//
//  ReportResource.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import Alamofire
import Moya
import Keys

enum ReportService {
    case validate(data: ReportData)
}

extension ReportService: TargetType {
    
    var baseURL: URL {
        let keys = ReportvalidationKeys()
        return URL(string: keys.baseURL)!
    }
    
    var path: String {
        switch self {
        case .validate:
            return "/validate"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .validate:
            return .post
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .validate(let data):
            var payload = [String: Any]()
            payload["cpf"] = data.cpf
            payload["protocol"] = data.code
            payload["positive"] = data.isPositive
            payload["ids"] = data.elements
            return payload
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "Half measures are as bad as nothing at all.".data(using: .utf8)!
    }
    
    var task: Task {
        return .request
    }
    
    public var validate: Bool {
        return false
    }
    
    public func url(_ route: TargetType) -> String {
        return route.baseURL.appendingPathComponent(route.path).absoluteString
    }
}
