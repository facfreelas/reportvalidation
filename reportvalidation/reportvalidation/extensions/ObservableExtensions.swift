//
//  ObservableExtensions.swift
//  tabletennismap
//
//  Created by Felipe Antonio Cardoso on 21/10/16.
//  Copyright © 2016 Table Tennis Map. All rights reserved.
//

import Foundation
import RxSwift

protocol OptionalType {
    associatedtype WrappedType
    func unwrap() -> WrappedType
    func hasValue() -> Bool
}

extension Optional: OptionalType {
    typealias WrappedType = Wrapped
    
    public func hasValue() -> Bool {
        return (self != nil)
    }
    
    func unwrap() -> WrappedType {
        return self!
    }
}

extension ObservableType where E: OptionalType {
    
    @warn_unused_result(message="http://git.io/rxs.uo")
    func notNil() -> Observable<E> {
        return self.filter {
            return $0.hasValue()
            
        }
    }
    
    @warn_unused_result(message="http://git.io/rxs.uo")
    func unwrap() -> Observable<E.WrappedType> {
        return self
            .filter { value in
                return value.hasValue()
            }
            .map { value -> E.WrappedType in
                value.unwrap()
        }
    }
}

extension ObservableType {
    @warn_unused_result(message="http://git.io/rxs.uo")
    func emptyMap() -> Observable<Void> {
        return self.map { _ in
            return
        }
    }
}

extension ObservableConvertibleType {
    
    func replaceWith<T>(value: T) -> Observable<T> {
        return asObservable()
            .flatMap { _ -> Observable<T> in
                .just(value)
        }
    }
}
