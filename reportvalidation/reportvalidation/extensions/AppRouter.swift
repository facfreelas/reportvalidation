//
//  AppRouter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 30/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import AVFoundation
import QRCodeReader
import AppRouter
import RxSwift

extension AppRouter {

    static func pushToUserDataViewController() {
        ValidatorViewController
            .presenterFromMainStoryboard()
            .push()
    }

    static func pushToReportElementsViewController(_ report: ReportRequiredData) {
        
        let factory = ReportOptionsFactory(withReport: report)
        
        ReportOptionsViewController
            .presenterFromMainStoryboard()
            .configure { $0.factory = factory }
            .push()
    }
    
    static func pushToReportValidationWithData(_ data: ReportRequiredData, andError error: PMError) {
        pushToReportValidationResult(ReportValidationErrorFactory(withData: data, andError: error))
    }
    
    static func pushToReportValidationWithData(_ data: ReportRequiredData, andItems items: [ReportItemDescription]) {
        pushToReportValidationResult(ReportValidationFactory(withData: data, andItems: items))
    }
    
    static func pushToReportValidationResult(_ factory: ReportValidationResultFabricable) {
        ReportValidationResultViewController
            .presenterFromMainStoryboard()
            .configure { $0.factory = factory }
            .push()
    }
    
    static func backToRoot() {
        _ = AppRouter.topViewController?.navigationController?.popToRootViewController(animated: true)
    }
    
    static func showQRCodeViewController() -> Observable<String?> {
        
        return Observable.create({ (observer) -> Disposable in
            
            guard QRCodeReader.isAvailable() else {
                observer.onError(PMError.cantReadBarCode)
                return Disposables.create()
            }
            
            let readerVC = QRCodeReaderViewController(builder:
                QRCodeReaderViewControllerBuilder {
                    $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode], captureDevicePosition: .back)
                }
            )
            
            readerVC.modalPresentationStyle = .formSheet
            readerVC.completionBlock = {
                readerVC.close()
                observer.onNext($0?.value)
                observer.onCompleted()
            }
            
            readerVC.presenter().present()
            return Disposables.create()
        })
    }
}

extension ARControllerConfigurableProtocol where Self: UIViewController {
    static func presenterFromMainStoryboard(_ initial: Bool = false) -> ViewControllerPresentConfiguration<Self> {
        return presenter().fromStoryboard("Main", initial: initial)
    }
}
