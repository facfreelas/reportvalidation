//
//  StringExtensions.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 31/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation

extension String {
    
    func isCPF() -> Bool {
        return matchRegex("([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})+")
        
    }
    
    func matchRegex(_ regex: String) -> Bool {
        return  NSPredicate(format:"SELF MATCHES %@", regex).evaluate(with: self)
    }
    
}

