//
//  UIViewControllerExtensions.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 08/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import SVProgressHUD

extension UIViewController {
    
    func showLoadingHUD(_ message: String) {
        SVProgressHUD.show(withStatus: message)
    }
    
    func showErrorHUD(_ message: String) {
        SVProgressHUD.showError(withStatus: message)
    }
    
    func removeHUD(_ delay: TimeInterval? = nil) {
        if let delay = delay {
            SVProgressHUD.dismiss(withDelay: delay)
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
}
