//
//  ResponseExtension.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import SwiftyJSON
import Moya

public extension Response {
    
    public func mapSwiftyJSON() -> JSON? {
        guard let json = try? mapJSON() else {
            return nil
        }
        return JSON(json)
    }
    
}
