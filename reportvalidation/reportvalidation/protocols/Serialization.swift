//
//  Serialization.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import SwiftyJSON

public protocol Serializable {
    
    func serialize() -> [String : Any]
}

public protocol Deserializable {
    
    init?(json: JSON)
}

extension Sequence where Self.Iterator.Element == JSON {
    
    public func deserialize<T: Deserializable>(initializer: ((JSON) -> T?)? = nil) -> [T] {
        return flatMap({ initializer?($0) ?? T(json: $0) })
    }
}
