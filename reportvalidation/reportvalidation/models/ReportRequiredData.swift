//
//  ReportRequiredData.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation


struct ReportRequiredData {
    var cpf: String
    var code: String
}
