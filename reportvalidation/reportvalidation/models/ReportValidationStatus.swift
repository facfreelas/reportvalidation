//
//  ReportValidation.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation

struct ReportValidationStatus {
    
    static func invalidReportStatus() -> ReportValidationStatus {
        return ReportValidationStatus(message: AppConfiguration.ErrorMessages.defaultErrorMessage,
                                      title: AppConfiguration.Titles.invalidReport,
                                      icon: AppConfiguration.Icons.errorIcon,
                                      color: AppConfiguration.Colors.errorRed)
    }
    
    static func validReportStatus() -> ReportValidationStatus {
        return ReportValidationStatus(message: "",
                                      title: AppConfiguration.Titles.validReport,
                                      icon: AppConfiguration.Icons.successIcon,
                                      color: AppConfiguration.Colors.successGreen)
    }
    
    var message: String
    var title: String?
    var icon: String?
    var color: String?
}
