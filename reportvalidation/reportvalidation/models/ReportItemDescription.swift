//
//  ReportItemDescription.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ReportItemDescription {
    var title: String
    var description: String
}

extension ReportItemDescription: Serializable, Deserializable {
    
    init?(json: JSON) {
        guard
            let title = json["title"].string,
            let description = json["description"].string
            else {
                return nil
        }
        self.title = title
        self.description = description
    }
    
    func serialize() -> [String : Any] {
        return [
            "title": self.title,
            "description": self.description
        ]
    }
    
}
