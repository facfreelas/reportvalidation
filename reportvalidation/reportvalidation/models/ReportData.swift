//
//  ReportData.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 31/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation

struct ReportData {
    var cpf: String
    var code: String
    var isPositive: Bool = false
    var elements: [Int]? = nil
}
