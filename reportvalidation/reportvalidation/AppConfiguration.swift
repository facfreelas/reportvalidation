//
//  AppConfiguration.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 21/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import UIKit
import HexColors

struct AppConfiguration {
    
    struct Messages {
        static let welcomeTitleMessage = "Bem vindo ao validador de laudos da Psychemedics"
        static let welcomeMessage = "Esse aplicativo irá ajudar a varificar a autenticidade dos laudos emitidos pela Psychemedics"
        static let validating = "Validando..."
    }
    
    struct ErrorMessages {
        static let cpfInvalid = "Digite um CPF válido"
        static let cantValidate = "Não foi possível realizar a validação. Por favor, verifique os dados e tente novamente."
        static let defaultErrorMessage = "As informações fornecidas não foram encontradas em nossa base de dados. Verifique se as informações estão corretas e se caso o erro persista, entre em contato com a Psychemedics (11) 3004-5411"
    }
    
    struct Buttons {
        static let barCodeButtonTitle = "LER CÓDIGO QR"
    }
    
    struct TextField {
        static let cpf = "CPF"
        static let code = "Protocolo"
    }
    
    struct Placeholders {
        static let cpf = "Digite o CPF"
        static let code = "Digite o protocolo do laudo"
    }
    
    struct Titles {
        static let welcomeScreen = "Bem vindo"
        static let reportDataScreen = "Informações"
        static let reportOptionsScreen = "Toxinas"
        static let reportScreen = "Validador de laudo"
        
        static let invalidReport = "LAUDO INVÁLIDO"
        static let validReport = "LAUDO VÁLIDO"
    }
    
    struct Colors {
        static let defaultColor = "#a9a9a9"
        static let mainColor = "#911c27"
        static let secundaryColor = "#ffffff"
        
        static let errorRed = "#ff0000"
        static let successGreen = "#419E72"
    }

    struct Masks {
        static let cpfMask = "[000].[000].[000]-[00]"
        static let reportNumberMask = "[___________________]"
    }
    
    struct Icons {
        static let errorIcon = "fa-exclamation-circle"
        static let successIcon = "fa-check-circle"
    }
    
}
