//
//  ReportValidationSections.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import RxDataSources

enum ReportValidationSectionModel {
    case StatusSection(title: String, items: [ReportItem])
    case ItemSection(title: String, items: [ReportItem])
}

enum ReportItem {
    case StatusItem(status: ReportValidationStatus)
    case ReportDataItem(item: ReportItemDescription)
}

extension ReportValidationSectionModel: SectionModelType {

    typealias Item = ReportItem
    
    var items: [ReportItem] {
        switch self {
        case .StatusSection(title: _, items: let items):
            return items.map{$0}
        case .ItemSection(title: _, items: let items):
            return items.map{$0}
        }
    }
    
    init(original: ReportValidationSectionModel, items: [Item]) {
        switch original {
        case let .StatusSection(title, _):
            self = .StatusSection(title: title, items: items)
        case let .ItemSection(title, _):
            self = .ItemSection(title: title, items: items)
        }
    }
    
}

extension ReportValidationSectionModel {
    var title: String {
        switch self {
        case .StatusSection(title: let title, items: _):
            return title
        case .ItemSection(title: let title, items: _):
            return title
        }
    }
}
