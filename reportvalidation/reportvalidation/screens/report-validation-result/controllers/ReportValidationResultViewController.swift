//
//  ReportValidationResultViewController.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ReportValidationResultViewController: UIViewController {

    @IBOutlet weak var containerView: ReportValidationResultContainerView!
    
    let disposeBag = DisposeBag()

    var presenter: ReportValidationResultPresentable!
    var factory: ReportValidationResultFabricable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = AppConfiguration.Titles.reportScreen
        setNeedsStatusBarAppearanceUpdate()
        
        presenter = factory.resolvePresenter(viewController: self)
        
        presenter
            .reportData()
            .bindTo(containerView.statusView.rx.reportData)
            .addDisposableTo(disposeBag)
        
        presenter
            .sections()
            .bindTo(containerView.items)
            .addDisposableTo(disposeBag)
        
        containerView
            .btnAction
            .rx.tap.asObservable()
            .bindTo(done)
            .addDisposableTo(disposeBag)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func showError(_ message: String) {}

    func showStatus(_ status: ReportValidationStatus) {
        
        let status = Observable.just(status).shareReplay(1)
        
        status
            .replaceWith(value: false)
            .bindTo(containerView.showContent)
            .addDisposableTo(disposeBag)
        
        status
            .bindTo(containerView.statusView.rx.status)
            .addDisposableTo(disposeBag)
    }
    
    func finalizeValidation() {
        presenter.finalizeValidation()
    }
}

extension ReportValidationResultViewController: ReportValidationResultView {
    
    var showStatus: AnyObserver<ReportValidationStatus> {
        return UIBindingObserver(UIElement: self, binding: { (controller, status) in
            controller.showStatus(status)
        }).asObserver()
    }
    
    var showError: AnyObserver<String> {
        return UIBindingObserver(UIElement: self, binding: { (controller, message) in
            controller.showError(message)
        }).asObserver()
    }
    
    var done: AnyObserver<Void> {
        return UIBindingObserver(UIElement: self, binding: { (controller, _) in
            controller.finalizeValidation()
        }).asObserver()
    }
}
