//
//  ReportValidationResultContainerView.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import RxCocoa
import NibDesignable
import FontAwesome_swift

@IBDesignable
class ReportValidationResultContainerView: NibDesignable, UITableViewDelegate {

    private let dataSource = RxTableViewSectionedReloadDataSource<ReportValidationSectionModel>()
    private let disposeBag = DisposeBag()

    @IBOutlet weak var statusView: ReportStatusView!
    @IBOutlet weak var tblItems: UITableView!
    @IBOutlet weak var btnAction: PMMainButton!
    
    var showContent: AnyObserver<Bool> {
        return UIBindingObserver(UIElement: self, binding: { (view, show) in
            view.showContent(show)
        }).asObserver()
    }
    
    var items = Variable<[ReportValidationSectionModel]>([])
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        
        tblItems.allowsMultipleSelection = true
        tblItems.delegate = self
        tblItems.estimatedRowHeight = 44.0
        tblItems.rowHeight = UITableViewAutomaticDimension
        
        tblItems.register(cellType: ReportStatusCell.self)
        tblItems.register(cellType: ReportItemDescriptionCell.self)
        
        skinTableViewDataSource(dataSource)
        
        let sections = items.asObservable().shareReplay(1)
        
        sections
            .filter{ $0.isEmpty }
            .replaceWith(value: true)
            .bindTo(showContent)
            .addDisposableTo(disposeBag)
        
        sections
            .bindTo(tblItems.rx.items(dataSource: dataSource))
            .addDisposableTo(disposeBag)
    }
    
    private func skinTableViewDataSource(_ dataSource: RxTableViewSectionedReloadDataSource<ReportValidationSectionModel>) {
        
        dataSource.configureCell = { [unowned self] ds, tv, ip, _ in

            switch self.dataSource[ip] {
                
            case let .StatusItem(status):
                return self.configureStatusCell(tv.dequeueReusableCell(for: ip), with: status)
                
            case let .ReportDataItem(item):
                return self.configureReportDataCell(tv.dequeueReusableCell(for: ip), with: item)
            }
        }
    }
    
    private func configureStatusCell(_ cell: ReportStatusCell, with status: ReportValidationStatus) -> ReportStatusCell {
        
        let color = UIColor(status.color ?? AppConfiguration.Colors.defaultColor) ?? UIColor.darkGray
        
        cell.lblTitle.text = status.title
        cell.lblTitle.textColor = color
        
        if let icon = status.icon, let code = String.fontAwesomeIconWithCode(icon), let fontName = FontAwesome(rawValue: code) {
            cell.imgIcon.image = UIImage.fontAwesomeIconWithName(fontName, textColor: color, size: CGSize(width: 80.0, height: 80.0))
        }
        
        return cell
    }
    
    private func configureReportDataCell(_ cell: ReportItemDescriptionCell, with item: ReportItemDescription) -> ReportItemDescriptionCell {
        cell.lblTitle.text = item.title
        cell.lblDescription.text = item.description
        return cell
    }
    
    
    func showContent(_ show: Bool) {
        tblItems.isHidden = !show
        statusView.isHidden = !tblItems.isHidden
    }
}
