//
//  ReportStatusCell.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import UIKit
import Reusable

final class ReportStatusCell: UITableViewCell, NibReusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
