//
//  ReportStatusView.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 08/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FontAwesome_swift
import HexColors
import SkyFloatingLabelTextField
import InputMask
import NibDesignable

@IBDesignable
class ReportStatusView: NibDesignable {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var txtCPF: SkyFloatingLabelTextField!
    @IBOutlet weak var txtProtocol: SkyFloatingLabelTextField!
    
    private let cpfListener: PolyMaskTextFieldDelegate = PolyMaskTextFieldDelegate(format: AppConfiguration.Masks.cpfMask)
    private let reportNumberListener: PolyMaskTextFieldDelegate = PolyMaskTextFieldDelegate(format: AppConfiguration.Masks.reportNumberMask)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cpfListener.delegate = self
        reportNumberListener.delegate = self
        
        txtCPF.lineColor = UIColor.clear
        txtProtocol.lineColor = UIColor.clear
    }
    
    func configureWithModel(_ status: ReportValidationStatus) {
        
        let color = UIColor(status.color ?? AppConfiguration.Colors.defaultColor) ?? UIColor.darkGray
        
        lblTitle.text = status.title
        lblTitle.textColor = color
        lblMessage.text = status.message
        
        if let icon = status.icon, let code = String.fontAwesomeIconWithCode(icon), let fontName = FontAwesome(rawValue: code) {
            imgIcon.image = UIImage.fontAwesomeIconWithName(fontName,
                                                            textColor: color,
                                                            size: CGSize(width: 80.0, height: 80.0)
            )
        }
    }
    
    func configureWithProtocolData(_ data: ReportRequiredData) {
        cpfListener.put(text: data.cpf, into: txtCPF)
        reportNumberListener.put(text: data.code, into: txtProtocol)
    }
}

extension Reactive where Base: ReportStatusView {
    var status: AnyObserver<ReportValidationStatus> {
        return UIBindingObserver(UIElement: base, binding: { (view, status) in
            view.configureWithModel(status)
        }).asObserver()
    }
    
    var reportData: AnyObserver<ReportRequiredData> {
        return UIBindingObserver(UIElement: base, binding: { (view, data) in
            view.configureWithProtocolData(data)
        }).asObserver()
    }
}
