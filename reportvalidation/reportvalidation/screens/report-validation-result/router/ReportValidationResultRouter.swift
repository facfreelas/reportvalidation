//
//  ReportValidationResultRouter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import AppRouter

class ReportValidationResultRouter: ReportValidationResultRouterable {
    func showRoot() {
        AppRouter.backToRoot()
    }
}
