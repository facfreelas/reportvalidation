//
//  ReportValidationResultInteractable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ReportValidationResultInteractable {
    
    var isValid: Observable<Bool> { get }
    
    func reportData() -> Observable<ReportRequiredData>
    func reportItems() -> Observable<[ReportItemDescription]>
    func statusData() -> Observable<ReportValidationStatus>
    func finalizeValidation()
}
