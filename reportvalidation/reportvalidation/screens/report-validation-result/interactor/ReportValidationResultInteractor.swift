//
//  ReportValidationResultInteractor.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift
import FontAwesome_swift

class ReportValidationResulErrortInteractor: ReportValidationResultInteractable {
    
    private let router: ReportValidationResultRouterable
    private let data: ReportRequiredData
    private let error: PMError
    
    var isValid: Observable<Bool> {
        return .just(false)
    }
    
    required init(router: ReportValidationResultRouterable, data: ReportRequiredData, error: PMError) {
        self.router = router
        self.data = data
        self.error = error
    }
    
    func reportData() -> Observable<ReportRequiredData> {
        return .just(data)
    }
    
    func reportItems() -> Observable<[ReportItemDescription]> {
        return .empty()
    }
    
    func statusData() -> Observable<ReportValidationStatus> {
        switch error {
        case .custom(data: let data):
            return .just(ReportValidationStatus(message: data.message, title: data.title, icon: data.icon, color: data.color))
        default:
            return .just(ReportValidationStatus.invalidReportStatus())
        }
    }
    
    func finalizeValidation() {
        router.showRoot()
    }
}

class ReportValidationResultInteractor: ReportValidationResultInteractable {
    
    private let router: ReportValidationResultRouterable
    private let data: ReportRequiredData
    private let items: [ReportItemDescription]
    
    var isValid: Observable<Bool> {
        return .just(items.count > 0)
    }
    
    required init(router: ReportValidationResultRouterable, data: ReportRequiredData, items: [ReportItemDescription]) {
        self.router = router
        self.data = data
        self.items = items
    }
    
    func reportData() -> Observable<ReportRequiredData> {
        return .just(data)
    }
    
    func reportItems() -> Observable<[ReportItemDescription]> {
        return Observable.just(items)
    }
    
    func statusData() -> Observable<ReportValidationStatus> {
        return isValid.map{ $0 ? ReportValidationStatus.validReportStatus() : ReportValidationStatus.invalidReportStatus() }
    }
    
    func finalizeValidation() {
        router.showRoot()
    }
}
