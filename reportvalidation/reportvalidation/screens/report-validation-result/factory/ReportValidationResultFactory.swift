//
//  ReportValidationResultFactory.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import Swinject


class ReportValidationErrorFactory: ReportValidationResultFabricable {

    private let error: PMError
    private let data: ReportRequiredData
    
    init(withData data: ReportRequiredData, andError error: PMError) {
        self.error = error
        self.data = data
    }
    
    func resolvePresenter(viewController: ReportValidationResultViewController) -> ReportValidationResultPresentable {
        
        let container = Container()
        
        container.register(ReportValidationResultInteractable.self) { (resolver) in
            return ReportValidationResulErrortInteractor(
                router: resolver.resolve(ReportValidationResultRouterable.self)!,
                data: self.data,
                error: self.error)
        }
        
        container.register(ReportValidationResultPresentable.self) { (resolver) in
            return ReportValidationResultPresenter(
                interactor: resolver.resolve(ReportValidationResultInteractable.self)!,
                view: resolver.resolve(ReportValidationResultView.self)!
            )
        }
        
        container.register(ReportValidationResultRouterable.self) { (resolver) in
            return ReportValidationResultRouter()
        }
        
        container.register(ReportValidationResultView.self) { (resolver) in
            return viewController
        }
        
        return container.resolve(ReportValidationResultPresentable.self)!
    }
}

class ReportValidationFactory: ReportValidationResultFabricable {
    
    private let items: [ReportItemDescription]
    private let data: ReportRequiredData
    
    init(withData data: ReportRequiredData, andItems items: [ReportItemDescription]) {
        self.items = items
        self.data = data
    }
    
    func resolvePresenter(viewController: ReportValidationResultViewController) -> ReportValidationResultPresentable {
        
        let container = Container()
        
        container.register(ReportValidationResultInteractable.self) { (resolver) in
            return ReportValidationResultInteractor(
                router: resolver.resolve(ReportValidationResultRouterable.self)!,
                data: self.data,
                items: items)
        }
        
        container.register(ReportValidationResultPresentable.self) { (resolver) in
            return ReportValidationResultPresenter(
                interactor: resolver.resolve(ReportValidationResultInteractable.self)!,
                view: resolver.resolve(ReportValidationResultView.self)!
            )
        }
        
        container.register(ReportValidationResultRouterable.self) { (resolver) in
            return ReportValidationResultRouter()
        }
        
        container.register(ReportValidationResultView.self) { (resolver) in
            return viewController
        }
        
        return container.resolve(ReportValidationResultPresentable.self)!
    }
}
