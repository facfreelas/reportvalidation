//
//  ReportValidationResultPresenter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

class ReportValidationResultPresenter: ReportValidationResultPresentable {
    
    let interactor: ReportValidationResultInteractable
    let view: ReportValidationResultView
    
    let disposeBag = DisposeBag()
    
    required init(interactor: ReportValidationResultInteractable, view: ReportValidationResultView) {
        self.interactor = interactor
        self.view = view
    }
    
    func reportData() -> Observable<ReportRequiredData> {
        return interactor.reportData()
    }
    
    func sections() -> Observable<[ReportValidationSectionModel]> {
        
        let valid = interactor.isValid.shareReplay(1)
        
        valid
            .filter{ !$0 }
            .withLatestFrom(interactor.statusData())
            .bindTo(view.showStatus)
            .addDisposableTo(disposeBag)
        
        let sections = Observable.combineLatest(interactor.statusData(), interactor.reportItems()) { (status, data) -> [ReportValidationSectionModel] in
            let reportDataItems = data.map({ (item) -> ReportItem in
                return .ReportDataItem(item: item)
            })
            
            let models: [ReportValidationSectionModel] = [
                .StatusSection(title: "", items: [.StatusItem(status: status)]),
                .ItemSection(title: "", items: reportDataItems)
            ]
            
            return models
        }
        
        return valid.filter{$0}.withLatestFrom(sections)
    }
    
    func finalizeValidation() {
        interactor.finalizeValidation()
    }
}
