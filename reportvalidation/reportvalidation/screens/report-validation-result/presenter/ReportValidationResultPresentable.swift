//
//  ReportValidationResultPresentable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ReportValidationResultPresentable {
    
    init(interactor: ReportValidationResultInteractable, view: ReportValidationResultView)
    
    func reportData() -> Observable<ReportRequiredData>
    func sections() -> Observable<[ReportValidationSectionModel]>
    func finalizeValidation() 
}
