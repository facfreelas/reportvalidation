//
//  MessageData.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation

protocol MessageDisplayable {
    var image: String { get }
    var title: String { get }
    var message: String { get }
    var callToAction: String { get }
}

struct MessageData: MessageDisplayable {
    var image: String
    var title: String
    var message: String
    var callToAction: String
}
