//
//  MessageInteractor.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

class MessageInteractor: MessageInteractable {

    let router: MessageRouterable
    let messageData: MessageDisplayable
    
    required init(router: MessageRouterable, messageData: MessageDisplayable) {
        self.router = router
        self.messageData = messageData
    }
    
    func performMessageAction() {
        router.sendUserDataScreen()
    }
    
    func dataFromMessage() -> Observable<MessageDisplayable> {
        return Observable.just(messageData).shareReplay(1)
    }
    
}
