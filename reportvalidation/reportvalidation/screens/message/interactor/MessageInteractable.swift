//
//  MessageInteractable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol MessageInteractable {
    
    init(router: MessageRouterable, messageData: MessageDisplayable)
    
    func dataFromMessage() -> Observable<MessageDisplayable>
    
    func performMessageAction()
}
