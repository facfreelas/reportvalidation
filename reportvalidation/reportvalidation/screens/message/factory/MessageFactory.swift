//
//  MessageFactory.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import Swinject

class MessageFactory: MessageFabricable {
    
    func resolvePresenter(viewController: MessageViewController) -> MessagePresentable {
        
        let container = Container()
        
        container.register(MessageRouterable.self) { (resolver) in
            return MessageRouter()
        }
        
        container.register(MessageInteractable.self) { (resolver) in
            return MessageInteractor(
                router: resolver.resolve(MessageRouterable.self)!,
                messageData: resolver.resolve(MessageDisplayable.self)!
            )
        }
        
        container.register(MessagePresentable.self) { (resolver) in
            return MessagePresenter(interactor: resolver.resolve(MessageInteractable.self)!)
        }
        
        container.register(MessageDisplayable.self) { (resolver) in
            return MessageData(
                image: "BarCode",
                title: AppConfiguration.Messages.welcomeTitleMessage,
                message: "Esse aplicativo irá ajudar a varificar a autenticidade dos laudos emitidos pela Psychemedics",
                callToAction: "INICIAR"
            )
        }
        
        return container.resolve(MessagePresentable.self)!
    }
    
}
