//
//  MessagePresentable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol MessagePresentable {
    
    init(interactor: MessageInteractable)
    
    func message() -> Observable<MessageDisplayable>
    
    func didTapAtActionButton()
    
}
