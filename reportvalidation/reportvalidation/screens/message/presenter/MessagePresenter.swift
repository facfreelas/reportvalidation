//
//  MessagePresenter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

class MessagePresenter: MessagePresentable {

    let interactor: MessageInteractable
    
    required init(interactor: MessageInteractable) {
        self.interactor = interactor
    }
    
    func message() -> Observable<MessageDisplayable> {
        return interactor.dataFromMessage()
    }
    
    func didTapAtActionButton() {
        interactor.performMessageAction()
    }
}
