//
//  MessageContainerView.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import UIKit
import NibDesignable

@IBDesignable
class MessageContainerView: NibDesignable {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAction: PMMainButton!
    
    func configureWithModel(_ model: MessageDisplayable) {
        imgLogo.image = UIImage(named: model.image)
        lblTitle.text = model.title
        lblMessage.text = model.message
        btnAction.setTitle(model.callToAction, for: .normal)
    }
}
