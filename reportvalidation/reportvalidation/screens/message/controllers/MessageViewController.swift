//
//  MessageViewController.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MessageViewController: UIViewController {

    @IBOutlet weak var containerView: MessageContainerView!
    
    let disposeBag = DisposeBag()
    
    var presenter: MessagePresentable?
    
    var rx_showMessage: AnyObserver<MessageDisplayable> {
        return UIBindingObserver(UIElement: self, binding: { (view, model) in
            view.containerView.configureWithModel(model)
        }).asObserver()
    }
    
    var rx_tapStart: AnyObserver<Void> {
        return UIBindingObserver(UIElement: self, binding: { (view, model) in
            view.presenter?.didTapAtActionButton()
        }).asObserver()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        title = AppConfiguration.Titles.welcomeScreen
        
        let factory = MessageFactory()
        presenter = factory.resolvePresenter(viewController: self)
    
        presenter?
            .message()
            .bindTo(rx_showMessage)
            .addDisposableTo(disposeBag)
    
        containerView
            .btnAction
            .rx.tap
            .bindTo(rx_tapStart)
            .addDisposableTo(disposeBag)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
