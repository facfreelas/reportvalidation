//
//  ReportOptionsPresentable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ReportOptionsPresentable {
    
    init(interactor: ReportOptionsInteractable, view: ReportOptionView) 
    
    func validate(_ data: ReportOptionData)
    func elementOptions() -> Observable<[ReportOptionSection]>
    
}
