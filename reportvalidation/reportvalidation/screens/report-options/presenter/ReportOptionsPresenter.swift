//
//  ReportOptionsPresenter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

class ReportOptionsPresenter: ReportOptionsPresentable {

    private let disposeBag = DisposeBag()
    
    let interactor: ReportOptionsInteractable
    let view: ReportOptionView
    
    required init(interactor: ReportOptionsInteractable, view: ReportOptionView) {
        self.interactor = interactor
        self.view = view
    }
    
    func validate(_ data: ReportOptionData) {
        
        Observable
            .just(true)
            .subscribe(view.showLoading)
            .addDisposableTo(disposeBag)
        
        let validationObservable = interactor
            .validate(data)
            .shareReplay(1)
        
        validationObservable
            .replaceWith(value: false)
            .subscribe(view.showLoading)
            .addDisposableTo(disposeBag)
        
        validationObservable
            .failureDisplayable()
            .map { $0.errorMessage }
            .subscribe(view.showError)
            .addDisposableTo(disposeBag)
        
        validationObservable
            .failureNotDisplayable()
            .subscribe(onNext: { [unowned self] (error) in self.interactor.invalidReport(error) } )
            .addDisposableTo(disposeBag)
    
        validationObservable
            .success()
            .subscribe(onNext: { [unowned self] (items) in self.interactor.validReport(items) } )
            .addDisposableTo(disposeBag)
    }
    
    func elementOptions() -> Observable<[ReportOptionSection]> {
        return interactor
            .drugs()
            .map { (drugs) -> [ReportOptionSection] in
                let items = drugs.flatMap { ReportOption(id: $0.id, title: $0.title) }
                return [ReportOptionSection(header: "", items: items)]
            }
    }
    
}


extension ObservableType where E == ValidationResult {
    
    func failureDisplayable() -> Observable<PMError> {
        return asObservable()
            .failure()
            .filter { (result) -> Bool in
                switch result {
                case .notFound, .custom:
                    return false
                default:
                    return true
                }
        }
    }
    
    func failureNotDisplayable() -> Observable<PMError> {
        return asObservable()
            .failure()
            .filter { (result) -> Bool in
                switch result {
                case .notFound, .custom:
                    return true
                default:
                    return false
                }
        }
    }
    
    func failure() -> Observable<PMError> {
        return asObservable()
            .filter { (result) -> Bool in
                switch result {
                case .failure:
                    return true
                default:
                    return false
                }
            }
            .flatMap({ (result) -> Observable<PMError> in
                switch result {
                case .failure(let error):
                    return .just(error)
                default:
                    return .empty()
                }
            })
    }
    
    
    func success() -> Observable<[ReportItemDescription]> {
        return asObservable()
            .filter { (result) -> Bool in
                switch result {
                case .success:
                    return true
                default:
                    return false
                }
            }
            .flatMap({ (result) -> Observable<[ReportItemDescription]> in
                switch result {
                case .success(let items):
                    return .just(items)
                default:
                    return .empty()
                }
            })
    }
}
