//
//  ReportOptionsViewController.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ReportOptionsViewController: UIViewController {
    
    @IBOutlet weak var containerView: ReportOptionsContainerView!
    
    let disposeBag = DisposeBag()
    
    var presenter: ReportOptionsPresentable!
    var factory: ReportOptionsFactory!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = AppConfiguration.Titles.reportOptionsScreen
        setNeedsStatusBarAppearanceUpdate()
        
        presenter = factory.resolvePresenter(viewController: self)
        
        containerView
            .data
            .bindTo(rx.validate)
            .addDisposableTo(disposeBag)
        
        presenter
            .elementOptions()
            .bindTo(containerView.rx.options)
            .addDisposableTo(disposeBag)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func validate(_ data: ReportOptionData) {
        presenter.validate(data)
    }
    
    func showLoading(_ show: Bool) {
        show ? showLoadingHUD(AppConfiguration.Messages.validating) : removeHUD()
    }
    
    func showError(_ message: String) {
        showErrorHUD(message)
        removeHUD(2.0)
    }
    
}

extension ReportOptionsViewController: ReportOptionView {
    
    var showLoading: AnyObserver<Bool> {
        return UIBindingObserver(UIElement: self, binding: { (controller, show) in
            controller.showLoading(show)
        }).asObserver()
    }
    
    var showError: AnyObserver<String> {
        return UIBindingObserver(UIElement: self, binding: { (controller, message) in
            controller.showError(message)
        }).asObserver()
    }
}

extension Reactive where Base: ReportOptionsViewController {
    
    var validate: AnyObserver<ReportOptionData> {
        return UIBindingObserver(UIElement: base, binding: { (controller, data) in
            controller.validate(data)
        }).asObserver()
    }
    
}
