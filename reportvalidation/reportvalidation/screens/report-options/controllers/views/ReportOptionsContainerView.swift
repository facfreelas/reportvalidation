//
//  ReportOptionsContainerView.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 03/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Reusable
import NibDesignable


@IBDesignable
class ReportOptionsContainerView: NibDesignable {

    @IBOutlet weak var scStatus: UISegmentedControl!
    @IBOutlet weak var lblDescriptionMessage: UILabel!
    @IBOutlet weak var tblOptions: UITableView!
    @IBOutlet weak var btnAction: PMMainButton!
    
    private let dataSource = RxTableViewSectionedReloadDataSource<ReportOptionSection>()
    private let viewModel = ReportOptionViewModel()
    private let disposeBag = DisposeBag()
    private var _data = Variable<ReportOptionData?>(nil)
    
    fileprivate var sections = Variable<[ReportOptionSection]>([])
    
    fileprivate var _selectedIndexPathsPlaceholder = Variable<[IndexPath]?>(nil)
    
    fileprivate var selectedOptionsPlaceholder: Observable<[ReportOption]?> {
        return _selectedIndexPathsPlaceholder
            .asObservable()
            .map({ (indexPaths) -> [ReportOption]? in
                return indexPaths?.flatMap({ [unowned self] (indexPath) -> ReportOption? in
                    return self.sections.value.first?.items[indexPath.row]
                })
            })
    }
    
    private var selectedIndexPaths: Observable<[IndexPath]?> {
        
        let selected = Observable.just(tblOptions.indexPathsForSelectedRows).shareReplay(1)
        
        selected
            .bindTo(_selectedIndexPathsPlaceholder)
            .addDisposableTo(disposeBag)
        
        return selected
    }
    
    private var selectedOptions: Observable<[ReportOption]?> {
        return selectedIndexPaths.map({ (indexPaths) -> [ReportOption]? in
            return indexPaths?.flatMap({ [unowned self] (indexPath) -> ReportOption? in
                return self.sections.value.first?.items[indexPath.row]
            })
        })
    }
    
    var data: Observable<ReportOptionData> {
        return _data.asObservable().unwrap()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        setupTableView()
    }

    private func setup() {
        
        selectedOptions
            .asObservable()
            .bindTo(viewModel.options)
            .addDisposableTo(disposeBag)
        
        let statusObservable = scStatus
            .rx.value
            .asObservable()
            .map { ($0 == 0) }
            .shareReplay(1)
        
        statusObservable
            .bindTo(viewModel.isPositive)
            .addDisposableTo(disposeBag)
    
        statusObservable
            .bindTo(rx.showSelectedOptions)
            .addDisposableTo(disposeBag)

        
        
        statusObservable
            .filter { $0 }
            .flatMap { [unowned self] (_) in self.selectedOptionsPlaceholder }
            .bindTo(viewModel.options)
            .addDisposableTo(disposeBag)
        
        statusObservable
            .filter { !$0 }
            .map { (_) in nil }
            .bindTo(viewModel.options)
            .addDisposableTo(disposeBag)
        
        viewModel
            .isValid
            .map { $0 ? 1.5 : 0.5 }
            .bindTo(btnAction.rx.alpha)
            .addDisposableTo(disposeBag)
        
        viewModel
            .isValid
            .bindTo(btnAction.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        viewModel
            .descriptionMessage
            .bindTo(lblDescriptionMessage.rx.text)
            .addDisposableTo(disposeBag)
        
        btnAction
            .rx.tap
            .bindTo(rx.action)
            .addDisposableTo(disposeBag)
    }
    
    private func setupTableView() {
        
        tblOptions.allowsMultipleSelection = true
        tblOptions.delegate = self
        tblOptions.tableFooterView = UIView()
        
        tblOptions.register(cellType: ReportOptionCell.self)
    
        tblOptions
            .rx.itemSelected.asObservable()
            .flatMap { [unowned self] (_) in self.selectedOptions }
            .bindTo(viewModel.options)
            .addDisposableTo(disposeBag)
        
        tblOptions
            .rx.itemDeselected.asObservable()
            .flatMap { [unowned self] (_) in self.selectedOptions }
            .bindTo(viewModel.options)
            .addDisposableTo(disposeBag)
        
        skinTableViewWithDataSource(dataSource)
        
        sections.asObservable()
            .bindTo(tblOptions.rx.items(dataSource: dataSource))
            .addDisposableTo(disposeBag)
    }
    
    private func skinTableViewWithDataSource(_ dataSource:  RxTableViewSectionedReloadDataSource<ReportOptionSection>) {
        dataSource.configureCell = { ds, tv, ip, item in
            let cell: ReportOptionCell  = tv.dequeueReusableCell(for: ip)
            cell.textLabel?.text = item.title
            return cell
        }
    }

    fileprivate func updateSections(_ sections: [ReportOptionSection]) {
        Observable
            .just(sections)
            .bindTo(self.sections)
            .addDisposableTo(disposeBag)
    }
    
    fileprivate func performAction() {
        Observable
            .just(viewModel.data)
            .bindTo(_data)
            .addDisposableTo(disposeBag)
    }
    
    fileprivate func showSelectedIndexPaths(_ show: Bool) {
        
        tblOptions.isUserInteractionEnabled = show
        tblOptions.isHidden = !show
        lblDescriptionMessage.isHidden = tblOptions.isHidden
        
        _selectedIndexPathsPlaceholder.value?.forEach({ [unowned self] (indexPath) in
            if show {
                self.tblOptions.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                self.tableView(self.tblOptions, didSelectRowAt: indexPath)
            } else {
                self.tblOptions.deselectRow(at: indexPath, animated: false)
                self.tableView(self.tblOptions, didDeselectRowAt: indexPath)
            }
        })
    }
}

extension ReportOptionsContainerView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .none
    }
    
}

extension Reactive where Base: ReportOptionsContainerView {
    
    fileprivate var action: AnyObserver<Void> {
        return UIBindingObserver(UIElement: base, binding: { (view, _) in
            view.performAction()
        }).asObserver()
    }
    
    var options: AnyObserver<[ReportOptionSection]> {
        return UIBindingObserver(UIElement: base, binding: { (view, sections) in
            view.updateSections(sections)
        }).asObserver()
    }
    
    var showSelectedOptions: AnyObserver<Bool> {
        return UIBindingObserver(UIElement: base, binding: { (view, show) in
            view.showSelectedIndexPaths(show)
        }).asObserver()
    }
}
