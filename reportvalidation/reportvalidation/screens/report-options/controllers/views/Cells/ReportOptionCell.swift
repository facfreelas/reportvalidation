//
//  ReportOptionCell.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 04/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import UIKit
import Reusable

final class ReportOptionCell: UITableViewCell, NibReusable {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
