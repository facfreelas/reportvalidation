//
//  ReportOptionSection.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 04/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import RxDataSources

struct ReportOptionSection {
    var header: String
    var items: [Item]
}

extension ReportOptionSection: SectionModelType {
    
    typealias Item = ReportOption
    
    init(original: ReportOptionSection, items: [Item]) {
        self = original
        self.items = items
    }
}
