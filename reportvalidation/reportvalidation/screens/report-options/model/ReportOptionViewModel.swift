//
//  ReportOptionViewModel.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 04/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

struct ReportOptionViewModel {
    
    var options = Variable<[ReportOption]?>(nil)
    var isPositive = Variable<Bool>(false)

    var descriptionMessage: Observable<String> {
        return .just("Marque abaixo quais toxinas constam como pertinentes ao laudo (se aplicável)")
    }
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(isPositive.asObservable(), options.asObservable()) {
            guard let reportOptions = $1 else {
                return !$0
            }
            return $0 ? (reportOptions.count > 0) : (reportOptions.count == 0)
        }
    }
    
    var data: ReportOptionData {
        return ReportOptionData(isPositive: isPositive.value, options: options.value)
    }
    
}
