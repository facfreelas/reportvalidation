//
//  ReportOption.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 04/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation

struct ReportOption {
    let id: Int
    let title: String
}
