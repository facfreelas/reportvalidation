//
//  ReportOptionsRouterable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ReportOptionsRouterable {
    func showReportValidationResultWithData(_ data: ReportRequiredData, andItems items: [ReportItemDescription])
    func showInvalidReportScreenWithData(_ data: ReportRequiredData, andError error: PMError)
}
