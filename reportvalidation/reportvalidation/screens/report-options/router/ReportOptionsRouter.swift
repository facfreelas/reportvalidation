//
//  ReportOptionsRouter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import AppRouter
import QRCodeReader

class ReportOptionsRouter: ReportOptionsRouterable {

    func showReportValidationResultWithData(_ data: ReportRequiredData, andItems items: [ReportItemDescription]) {
        AppRouter.pushToReportValidationWithData(data, andItems: items)
    }
    
    func showInvalidReportScreenWithData(_ data: ReportRequiredData, andError error: PMError) {
        AppRouter.pushToReportValidationWithData(data, andError: error)
    }
}
