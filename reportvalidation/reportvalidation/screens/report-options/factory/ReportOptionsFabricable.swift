//
//  ReportOptionsFabricable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation

protocol ReportOptionsFabricable {
    func resolvePresenter(viewController: ReportOptionsViewController) -> ReportOptionsPresentable
}
