//
//  ReportOptionsFactory.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import Moya
import Swinject

class ReportOptionsFactory: ReportOptionsFabricable {
    
    private let report: ReportRequiredData
    
    init(withReport report: ReportRequiredData) {
        self.report = report
    }
    
    func resolvePresenter(viewController: ReportOptionsViewController) -> ReportOptionsPresentable {
        
        let container = Container()
        
        container.register(ReportOptionsRouterable.self) { (resolver) in
            return ReportOptionsRouter()
        }
        
        container.register(ReportOptionsInteractable.self) { (resolver) in
            return ReportOptionsInteractor(
                router: resolver.resolve(ReportOptionsRouterable.self)!,
                reportData: self.report,
                provider: resolver.resolve(ReportDataProvider.self)!
            )
        }
        
        container.register(ReportOptionsPresentable.self) { (resolver) in
            return ReportOptionsPresenter(
                interactor: resolver.resolve(ReportOptionsInteractable.self)!,
                view: resolver.resolve(ReportOptionView.self)!
            )
        }
        
        container.register(ReportDataProvider.self) { (resolver) in
            return ReportWebService(service: RxMoyaProvider<ReportService>())
        }
        
        container.register(ReportOptionView.self) { (resolver) in
            return viewController
        }
        
        return container.resolve(ReportOptionsPresentable.self)!
    }
    
}
