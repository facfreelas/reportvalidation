//
//  ReportOptionsInteractable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ReportOptionsInteractable {
    
    init(router: ReportOptionsRouterable, reportData: ReportRequiredData, provider: ReportDataProvider)

    func drugs() -> Observable<[Drug]>
    func validate(_ data: ReportOptionData) -> Observable<ValidationResult>
    func validReport(_ items: [ReportItemDescription])
    func invalidReport(_ error: PMError)
}
