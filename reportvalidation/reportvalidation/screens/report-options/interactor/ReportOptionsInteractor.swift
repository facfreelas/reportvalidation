//
//  ReportOptionsInteractor.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

class ReportOptionsInteractor: ReportOptionsInteractable {
    
    let router: ReportOptionsRouterable
    var reportData: ReportRequiredData
    let provider: ReportDataProvider
    
    required init(router: ReportOptionsRouterable, reportData: ReportRequiredData, provider: ReportDataProvider) {
        self.router = router
        self.reportData = reportData
        self.provider = provider
    }
    
    func drugs() -> Observable<[Drug]> {
        return .just([
                Drug(id: 1, title: "Cocaína"),
                Drug(id: 2, title: "Maconha"),
                Drug(id: 3, title: "Anfetamina"),
                Drug(id: 4, title: "Metanfetamina"),
                Drug(id: 5, title: "Ecstasy"),
                Drug(id: 6, title: "Opiáceos")
            ])
    }
    
    func validate(_ data: ReportOptionData) -> Observable<ValidationResult> {
        
        let data = ReportData(
            cpf: reportData.cpf,
            code: reportData.code,
            isPositive: data.isPositive,
            elements: data.options?.map{ $0.id }
        )
        
        return provider.validateReport(data)
    }
    
    func validReport(_ items: [ReportItemDescription]) {
        router.showReportValidationResultWithData(self.reportData, andItems: items)
    }

    func invalidReport(_ error: PMError) {
        router.showInvalidReportScreenWithData(self.reportData, andError: error)
    }
}
