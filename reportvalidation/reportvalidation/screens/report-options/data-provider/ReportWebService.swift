//
//  ReportDataProvider.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import Result
import RxSwift
import SwiftyJSON
import Moya

class ReportWebService: ReportDataProvider {
    
    private let service: RxMoyaProvider<ReportService>
    
    required init(service: RxMoyaProvider<ReportService>){
        self.service = service
    }

    func validateReport(_ data: ReportData) -> Observable<ValidationResult> {
        return service
            .request(.validate(data: data))
            .map { [weak self] (response) -> ValidationResult in
                
                guard let strongSelf = self else {
                    return ValidationResult(error: .unknow)
                }
                
                guard let json = response.mapSwiftyJSON(), let fields = json["fields"].array, response.statusCode == 200 else {
                    return ValidationResult(error: strongSelf.getErrorFromResponse(response))
                }
                
                return ValidationResult(value: fields.deserialize() as [ReportItemDescription])
            }
    }
    
    func getErrorFromResponse(_ response: Response) -> PMError {
        switch response.statusCode {
        case 404:
            return .notFound
        case 400:
            return .badRequest(message: AppConfiguration.ErrorMessages.cantValidate)
        case 409:
            guard let json  = response.mapSwiftyJSON(), let data = ([json].deserialize() as [ErrorData]).first else {
                return .unknow
            }
            return .custom(data: data)
        default:
            return .unknow
        }
    }
}

