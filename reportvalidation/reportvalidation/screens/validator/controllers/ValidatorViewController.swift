//
//  ValidatorViewController.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ValidatorViewController: UIViewController {

    @IBOutlet weak var containerView: ValidatorContainerView!
    
    let disposeBag = DisposeBag()
    
    var presenter: ValidatorPresentable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = AppConfiguration.Titles.reportDataScreen
        setNeedsStatusBarAppearanceUpdate()
        
        let factory = ValidatorFactory()
        presenter = factory.resolvePresenter(viewController: self)
        
        containerView
            .btnScanReportNumber
            .rx.tap
            .bindTo(rx.scanBarCode)
            .addDisposableTo(disposeBag)
        
        containerView
            .isValid
            .bindTo(containerView.btnAction.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        containerView
            .btnAction
            .rx.tap.bindTo(rx.action)
            .addDisposableTo(disposeBag)
        
        presenter
            .screenDataModel()
            .bindTo(rx.configureContainerView)
            .addDisposableTo(disposeBag)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    fileprivate func scan() {
        presenter
            .scanBarCode()
            .unwrap()
            .bindTo(containerView.viewModel.code)
            .addDisposableTo(disposeBag)
    }
    
    fileprivate func continueValidation() {
        containerView
            .formData()
            .bindTo(rx.continueValidation)
            .addDisposableTo(disposeBag)
    }
    
    func showError(_ message: String) {
        showErrorHUD(message)
        removeHUD(2.0)
    }
    
    func showLoading(_ show: Bool) {}
}

extension ValidatorViewController: ValidatorView {
    
    var showError: AnyObserver<String> {
        return UIBindingObserver(UIElement: self, binding: { (controller, message) in
            controller.showError(message)
        }).asObserver()
    }
    
    var showLoading: AnyObserver<Bool> {
        return UIBindingObserver(UIElement: self, binding: { (controller, show) in
            controller.showLoading(show)
        }).asObserver()
    }
    
}

extension Reactive where Base: ValidatorViewController {
    
    var configureContainerView: AnyObserver<ValidatorScreenInfoDisplayable> {
        return UIBindingObserver(UIElement: base, binding: { (view, model) in
            view.containerView.configureWithModel(model)
        }).asObserver()
    }
    
    var scanBarCode: AnyObserver<Void> {
        return UIBindingObserver(UIElement: base, binding: { (view, model) in
            view.scan()
        }).asObserver()
    }
    
    var action: AnyObserver<Void> {
        return UIBindingObserver(UIElement: base, binding: { (view, model) in
            view.continueValidation()
        }).asObserver()
    }
    
    var continueValidation: AnyObserver<ValidatorFormData> {
        return UIBindingObserver(UIElement: base, binding: { (view, data) in
            view.presenter.continueWithData(data)
        }).asObserver()
    }
}
