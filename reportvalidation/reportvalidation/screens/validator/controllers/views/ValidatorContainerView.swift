//
//  ValidatorContainerView.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import InputMask
import NibDesignable

@IBDesignable
class ValidatorContainerView: NibDesignable {

    @IBOutlet weak var txtCPF: PMTextField!
    @IBOutlet weak var txtReportNumber: PMTextField!
    @IBOutlet weak var btnScanReportNumber: UIButton!
    @IBOutlet weak var btnAction: PMMainButton!

    private let cpfListener: PolyMaskTextFieldDelegate = PolyMaskTextFieldDelegate(format: AppConfiguration.Masks.cpfMask)
    private let reportNumberListener: PolyMaskTextFieldDelegate = PolyMaskTextFieldDelegate(format: AppConfiguration.Masks.reportNumberMask)
    
    fileprivate let disposeBag = DisposeBag()
    
    let viewModel = ValidatorViewModel()
    
    var isValid: Observable<Bool> {
        return viewModel.isValid
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        
        cpfListener.delegate = self
        txtCPF.delegate = cpfListener
        txtCPF.keyboardType = .numberPad
        txtCPF.placeholder = AppConfiguration.Placeholders.cpf
        txtCPF.title = AppConfiguration.TextField.cpf
        
        reportNumberListener.delegate = self
        txtReportNumber.placeholder = AppConfiguration.Placeholders.code
        txtReportNumber.title = AppConfiguration.TextField.code
        txtReportNumber.delegate = reportNumberListener
        
        btnScanReportNumber.setTitle(AppConfiguration.Buttons.barCodeButtonTitle, for: .normal)
        
        viewModel.isValid
            .map {$0 ? 1.0 : 0.5}
            .bindTo(btnAction.rx.alpha)
            .addDisposableTo(disposeBag)
        
        viewModel.isValid
            .bindTo(btnAction.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        viewModel
            .code.asObservable()
            .bindTo(txtReportNumber.rx.text)
            .addDisposableTo(disposeBag)
    }
    
    func configureWithModel(_ model: ValidatorScreenInfoDisplayable) {
        btnAction.setTitle(model.btnActionTitle, for: .normal)
        btnScanReportNumber.setTitle(model.btnScanReportCodeTitle, for: .normal)
    }
    
    func formData() -> Observable<ValidatorFormData> {
        return viewModel.data
    }
    
}

extension ValidatorContainerView: MaskedTextFieldDelegateListener {
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        
        if txtCPF.isEqual(textField) {
            
            Observable.just(value)
                .bindTo(viewModel.cpf)
                .addDisposableTo(disposeBag)
            
        } else if txtReportNumber.isEqual(textField) {
            
            Observable.just(value)
                .bindTo(viewModel.code)
                .addDisposableTo(disposeBag)
            
        }
    }
}
