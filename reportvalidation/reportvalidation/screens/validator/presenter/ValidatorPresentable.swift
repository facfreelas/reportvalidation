//
//  ValidatorPresentable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ValidatorPresentable {
    
    init(interactor: ValidatorInteractable, view: ValidatorView)
    
    func screenDataModel() -> Observable<ValidatorScreenInfoDisplayable>
    func continueWithData(_ data: ValidatorFormData)
    func scanBarCode() -> Observable<String?>
}
