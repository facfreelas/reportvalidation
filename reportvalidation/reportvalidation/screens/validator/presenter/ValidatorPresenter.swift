//
//  ValidatorPresenter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

class ValidatorPresenter: ValidatorPresentable {

    private let dipsoseBag = DisposeBag()
    
    let interactor: ValidatorInteractable
    let view: ValidatorView
    
    required init(interactor: ValidatorInteractable, view: ValidatorView) {
        self.interactor = interactor
        self.view = view
    }
    
    func screenDataModel() -> Observable<ValidatorScreenInfoDisplayable> {
        return interactor.screenData()
    }
    
    func continueWithData(_ data: ValidatorFormData) {
        let reportData = ReportRequiredData(cpf: data.cpf, code: data.code)
        return interactor.continueWithReportData(reportData)
    }

    func scanBarCode() -> Observable<String?> {
        return interactor
            .barCode()
            .catchError { [unowned self] (error) -> Observable<String?> in
                
                if let pmError = error as? PMError {
                    Observable.just(pmError.errorMessage)
                        .subscribe(self.view.showError)
                        .addDisposableTo(self.dipsoseBag)
                } else {
                    Observable.just(PMError.unknow.errorMessage)
                        .subscribe(self.view.showError)
                        .addDisposableTo(self.dipsoseBag)
                }
                
                return .just(nil)
            }
    }
}
 
