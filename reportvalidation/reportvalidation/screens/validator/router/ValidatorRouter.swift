//
//  ValidatorRouter.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift
import AppRouter
import QRCodeReader

class ValidatorRouter: ValidatorRouterable {
    
    func showElementOptionsForReport(_ report: ReportRequiredData) {
        return AppRouter.pushToReportElementsViewController(report)
    }
    
    func showQRCodeReader() -> Observable<String?> {
        return AppRouter.showQRCodeViewController()
    }
    
}
