//
//  ValidatorRouterable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ValidatorRouterable {
    func showElementOptionsForReport(_ report: ReportRequiredData)
    func showQRCodeReader() -> Observable<String?>
}
