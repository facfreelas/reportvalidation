//
//  ValidatorInteractor.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

class ValidatorInteractor: ValidatorInteractable {

    let router: ValidatorRouterable
    let screenDataModel: ValidatorScreenInfoDisplayable
    
    required init(router: ValidatorRouterable, screenDataModel: ValidatorScreenInfoDisplayable) {
        self.router = router
        self.screenDataModel = screenDataModel
    }
    
    func screenData() -> Observable<ValidatorScreenInfoDisplayable> {
        return .just(screenDataModel)
    }
    
    func continueWithReportData(_ data: ReportRequiredData) {
        return router.showElementOptionsForReport(data)
    }
    
    func barCode() -> Observable<String?> {
        return router.showQRCodeReader()
    }
    
}
