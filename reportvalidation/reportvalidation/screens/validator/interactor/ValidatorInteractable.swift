//
//  ValidatorInteractable.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

protocol ValidatorInteractable {
    
    init(router: ValidatorRouterable, screenDataModel: ValidatorScreenInfoDisplayable)
    
    func screenData() -> Observable<ValidatorScreenInfoDisplayable>
    func continueWithReportData(_ data: ReportRequiredData)
    func barCode() -> Observable<String?>
}
