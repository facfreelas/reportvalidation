//
//  ValidatorViewModel.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 03/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import RxSwift

struct ValidatorViewModel {

    private var cpfErrorMessage = Variable<String?>(nil)
    private var codeErrorMessage = Variable<String?>(nil)
    private var _isValid = Variable<Bool>(false)
    private let diposeBag = DisposeBag()
    
    var cpf = Variable<String?>(nil)
    var code = Variable<String?>(nil)
    
    var cpfErrorMessageObservable: Observable<String?> {
        return cpfErrorMessage.asObservable()
    }
    
    var codeErrorMessageObservable: Observable<String?> {
        return codeErrorMessage.asObservable()
    }
    
    var isCPFValid: Observable<Bool> {
        return cpf.asObservable()
            .unwrap()
            .map { !$0.isEmpty }
    }

    var isCodeValid: Observable<Bool> {
        return code.asObservable()
            .unwrap()
            .map { !$0.isEmpty }
    }
    
    var isValid: Observable<Bool> {
        return _isValid.asObservable()
    }
    
    var data: Observable<ValidatorFormData> {
        
        guard let cpf = cpf.value, let code = code.value else {
            return .empty()
        }
        
        return .just(ValidatorFormData(cpf: cpf,code: code))
    }
    
    init() {
        Observable
            .combineLatest(isCPFValid, isCodeValid) { $0 && $1 }
            .bindTo(_isValid)
            .addDisposableTo(diposeBag)
    }
    
}
