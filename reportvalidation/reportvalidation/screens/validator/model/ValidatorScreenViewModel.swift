//
//  ValidatorViewModel.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 30/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation

protocol ValidatorScreenInfoDisplayable {
    var btnActionTitle: String { get }
    var btnScanReportCodeTitle: String { get }
}

struct ValidatorScreenInfo: ValidatorScreenInfoDisplayable {
    var btnActionTitle: String
    var btnScanReportCodeTitle: String
}
