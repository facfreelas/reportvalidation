//
//  ValidatorFormData.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 30/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation

struct ValidatorFormData {
    var cpf: String
    var code: String
}
