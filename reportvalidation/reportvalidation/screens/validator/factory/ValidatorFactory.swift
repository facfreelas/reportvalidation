//
//  ValidatorFactory.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 27/12/16.
//  Copyright © 2016 rvcompany. All rights reserved.
//

import Foundation
import Swinject

class ValidatorFactory: ValidatorFabricable {
    
    func resolvePresenter(viewController: ValidatorViewController) -> ValidatorPresentable {
        
        let container = Container()
        
        container.register(ValidatorRouterable.self) { (resolver) in
            return ValidatorRouter()
        }
        
        container.register(ValidatorInteractable.self) { (resolver) in
            return ValidatorInteractor(
                router: resolver.resolve(ValidatorRouterable.self)!,
                screenDataModel: resolver.resolve(ValidatorScreenInfoDisplayable.self)!
            )
        }
        
        container.register(ValidatorPresentable.self) { (resolver) in
            return ValidatorPresenter(
                interactor: resolver.resolve(ValidatorInteractable.self)!,
                view: resolver.resolve(ValidatorView.self)!)
        }
        
        container.register(ValidatorScreenInfoDisplayable.self) { (resolver) in
            return ValidatorScreenInfo (
                btnActionTitle: "CONTINUAR",
                btnScanReportCodeTitle: "LER CÓDIGO QR"
            )
        }
        
        container.register(ValidatorView.self) { (resolver) in
            return viewController
        }
        
        return container.resolve(ValidatorPresentable.self)!
    }
    
}
