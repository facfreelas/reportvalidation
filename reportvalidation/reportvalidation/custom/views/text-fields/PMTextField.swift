//
//  PMTextField.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 08/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import UIKit
import HexColors
import SkyFloatingLabelTextField

class PMTextField: SkyFloatingLabelTextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tintColor = UIColor.darkGray
        font = font?.withSize(20.0)
        
        textColor = UIColor.darkGray
        lineColor = tintColor
        lineHeight = 1.0
    
        selectedLineHeight = 1.0
        selectedTitleColor = UIColor.darkGray
        selectedLineColor = UIColor.darkGray
        
        if let color = UIColor(AppConfiguration.Colors.mainColor) {
            selectedTitleColor = color
            selectedLineColor = color
        }
    }
}
