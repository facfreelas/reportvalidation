//
//  PMMainButton.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 08/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import UIKit
import HexColors

class PMMainButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        backgroundColor = UIColor(AppConfiguration.Colors.mainColor)
        setTitleColor(UIColor(AppConfiguration.Colors.secundaryColor), for: .normal)
    }
}
