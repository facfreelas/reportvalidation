//
//  PMError.swift
//  reportvalidation
//
//  Created by Felipe Antonio Cardoso on 07/01/17.
//  Copyright © 2017 rvcompany. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ErrorData {
    var message: String
    var icon: String?
    var title: String?
    var color: String?
}

extension ErrorData: Deserializable {
    
    init?(json: JSON) {
        
        guard let message = json["error"].string else {
            return nil
        }

        self.icon = json["icon"].string ?? AppConfiguration.Icons.errorIcon
        self.title = json["title"].string ?? AppConfiguration.Titles.invalidReport
        self.color = json["color"].string ?? AppConfiguration.Colors.errorRed
        self.message = message
    }
    
}

enum PMError: Error {
    
    case cantReadBarCode
    case badRequest(message: String)
    case notFound
    case custom(data: ErrorData)
    case unknow
    
    var errorMessage: String {
        switch self {
        case .cantReadBarCode:
            return "Não é possivel ler o código de barras"
        case .badRequest(let message):
            return message
        case .notFound:
            return "Não encontrado"
        case .custom(let data):
            return data.message
        case .unknow:
            return "Erro desconhecido"
        }
    }
}
